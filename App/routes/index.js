/*
  /routes/index.js
*/
const express = require('express');
const router = express.Router();
const path = require('path');

// Importem controladors
var ctrlDir = path.resolve("Controllers/");
// Importem controlador de user
var userCtrl = require(path.join(ctrlDir, "user"));
var chatCtrl = require(path.join(ctrlDir, "chat"));

//Middleware para mostrar datos del request
router.use(function (req, res, next) {
    console.log('/' + req.method);
    next();
});


//      
// HOME 
//      
router.route("/").get(async(req, res, next) => {
    res.render("index");
});

//
// REGISTER view
//
router.route("/register").get(async(req, res, next) => {
    res.render("register");
});

// REGISTER POST
router.post('/register', userCtrl.register);

//
// LOGIN post
//
router.post("/", userCtrl.login);

//
// LOGOUT 
// 
router.post('/logout', userCtrl.logout);

//
// HISTORY
//
router.route("/history").get(async(req, res, next) => {
    res.render("history");
});

//
// CHAT LIST
//
router.route("/chatList").get(async(req, res, next) => {
    res.render("chatList");
});
//
// SALA CHAT
//
router.route("/chat/view").post(async(req, res, next) => {
    console.log(req.body.chatId);
    res.render("salaChat", {chatId: req.body.chatId});
});

//
// HISTORIAL CHAT
//
router.route("/history/view").get(async(req, res, next) => {
    res.render("salaHistory");
});
  
//Link routes and functions

// router.post('/user', userCtrl.add);
// router.get('/users', userCtrl.list);
// router.get('/user/:id', userCtrl.find);

module.exports = router;