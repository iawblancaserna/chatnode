//controllers/user.js

var User = require('../models/user');

//Create a new user and save it
var register = (req, res, next) => {
    var user = new User({ username: req.body.name, password: req.body.paswd });
    user.save();
    console.log(user.username);
    console.log(user.password);
    res.redirect("/");
    // return user;
};

// Find person by id
var find = (req, res) => {
    // Busquem per el nom de l'usuari
    User.findOne({ username: req.body.name }, function (error, user) {
        return user;
    })
};

// _id --> id en mongodb
//  var user = await User.find({username.body.user});

var login = async (req, res, next) => {
    console.log(req.session);
    const user = await User.findOne({username: req.body.name});
    if (!user) {
        return res.json({
            msg: "User not found!!"
        });
    }
    if (user.password != req.body.paswd) {
        return res.json({
            status: 0,
            msg: "Wrong password. Try again"
        });
    }

    req.session.id = user.id;

    res.redirect("/chatList"); // li enviem a la llista de chats
}

var getNameUser = (req, res, next) => {
    let username = req.session.name;
    return "holasession";
}

var loginCheck = async (req, res, next) => {
    req.session.userid;
}

var logout = async (req, res, next) => {
    req.session.destroy();
    res.redirect('/');
}

module.exports = {
    register,
    find,
    login,
    logout,
    getNameUser
}