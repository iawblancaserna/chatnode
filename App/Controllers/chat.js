//Inicializamos el modulo de mongoose y vingulamos el modelo Chat (necesario para operar con el Documento chats
var mongoose = require("mongoose"),
Chat = require("../models/chat");
//Al tratarse de acceso a BD la función es ahora ASYNC
exports.load = async(req, res, next) => {

   try {
       const results = await Chat.find({});
       //If is API request we want the json
       //but if is web request we don't have to define the res object as 200
       if (req.isApi) res.status(200).jsonp(results);
       else return results;
    } catch (error) {
         console.log(`Could not fetch chat ${error}`);
    }
};

// chatController.save = function (req, res) {
//   var chat = new Chat(req.body);
//   chat.save(function (err, results) {
//     if (err) res.send(500, err.message);
//     console.log("Successfully created a chat. :)");
//     res.status(200).jsonp(results);
//   });
// };
