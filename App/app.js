//app.js
var express = require('express');
var session = require('express-session');
router = express.Router();
var app = express();
var mongoose = require("mongoose");
var path1 = require('path');

const index = require('./routes/index');
const path = __dirname + 'public';
const public = path.join(__dirname + '/public/');

app.use(express.static(public));

// Session middleware
app.use(session({
  secret: 'cat',
  resave: false,
  saveUninitialized: true
}));
app.use(router);
// Parsing the incoming data
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.set('view engine', 'pug');

app.use(express.static(path));

app.use('/', index);

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

// CONECTAR MONGO DB
mongoose.connect(
  `mongodb://root:pass12345@mongodb:27017/tutorial?authSource=admin`,
  { useNewUrlParser: true },
  (err, res) => {
    if (err) console.log(`ERROR: connecting to Database.  ${err}`);
    else console.log(`Database Online`);
  }
);
const port = process.env.SERVER_PORT || 6000;
var server = require("http").createServer(app);
server.listen(port, (err, res) => {
  if (err) console.log(`ERROR: Connecting APP ${err}`);
  else console.log(`Server is running on port ${port}`);
});

var socketsRouter = require("./routes/index");
var handlerError = require("./routes/handler");

// app.use(express.static(path.join(__dirname, 'app')));
// Define routes using URL path
app.use("/", socketsRouter);
app.use(handlerError);

//app.js
require("./io");

