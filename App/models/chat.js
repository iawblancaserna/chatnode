var mongoose = require("mongoose"),
Schema = mongoose.Schema;

//Unica estructura permitida para ser escrita y leida en la base de datos
var chatSchema = new Schema({
   nick: { type: String },
   text: { type: String },
   msgs: { type: [Object] },
   created_at: { type: Date, default: Date.now }
});
module.exports = mongoose.model("Chat", chatSchema);
